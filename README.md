#### A Sample ASP.NET Core Web Application with Angular frontend

This is a barebones application that will provide a starting point for using the Edinburgh festivals API.
The [API documentation](https://api.edinburghfestivalcity.com/documentation#authentication) is sufficient to get started, but this will provide an actual implementation which you are free to follow.

Key areas of interest are:
`FestivalDataController.cs` and `home.component.ts`
These files contain the the logic around building and authenticating a request against the API.