import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  _baseUrl: string;

  public results = new Array<any>();

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl;
  }

  public searchEvents($event) {
    this.http.get(this._baseUrl + `api/FestivalData/Search?query=${$event}`)
      .subscribe((response: any[]) => this.results = response);
  }

  public getMapLocation(latitude, longitude) {
    return 'https://api.opencagedata.com/geocode/v1/json?q='
      + latitude
      + '+'
      + longitude
      + '&key=d557fcdd0991442b8c89bddeae45a8cb&format=map';
  }
}
