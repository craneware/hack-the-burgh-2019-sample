﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using System.Net.Http.Headers;

namespace Festival.Sample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FestivalDataController : ControllerBase
    {
        private readonly IHttpClientFactory _clientFactory;

        const string API_KEY = "V37UcPDfsVDiaYgs";
        const string API_SECRET = "yOjBov7vc-0op26qkHmaLOL4abzb34oK";
        const string BASE_URL = "https://api.edinburghfestivalcity.com";

        public FestivalDataController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<string>> Search(string query)
        {
            var encodedQuery = Uri.EscapeDataString(query);
            var queryWithKey = $"/events?title={encodedQuery}&key={API_KEY}";
            var signature = CreateSignature(queryWithKey, API_SECRET);
            var authenticationParameters = new Dictionary<string, string>
            {
                { "signature", signature }
            };

            var authenticatedUri = QueryHelpers.AddQueryString(BASE_URL + queryWithKey, authenticationParameters);

            var request = new HttpRequestMessage(HttpMethod.Get, authenticatedUri);
            var client = _clientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStreamAsync();
                using (var streamReader = new StreamReader(result, Encoding.UTF8))
                {
                    var content = streamReader.ReadToEnd();
                    return Ok(content);
                }
            }

            return Unauthorized();
        }

        private static string CreateSignature(string query, string secret)
        {
            var encoding = new ASCIIEncoding();
            byte[] keyBytes = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(query);

            using (var hmacsha1 = new HMACSHA1(keyBytes))
            {
                byte[] hashMessage = hmacsha1.ComputeHash(messageBytes);
                return ByteToString(hashMessage);
            }
        }

        private static string ByteToString(byte[] buff)
        {
            string binary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                binary += buff[i].ToString("x2");
            }
            return (binary);
        }
    }
}